import kotlinx.coroutines.*

suspend fun myFun() {
    delay(5000L)
}

fun main() = runBlocking {
    var deffered = GlobalScope.async {
        myFun()
        return@async 10L + 20L
    }
    println(deffered.await())
    var job = GlobalScope.launch {
        // launch a new coroutine in background and continue
        delay(1000L) // non-blocking delay for 1 second (default time unit is ms)
        println("World!") // print after delay
    }
    println("Hello,") // main thread continues while coroutine is delayed
    Thread.sleep(2000L) // block main thread for 2 seconds to keep JVM alive
    runBlocking {
        job.join()
    }
}